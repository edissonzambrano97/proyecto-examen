﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    //Declaramos Motor comun e implementamos la clase abstracta Motor para el respectivo funcionamiento del mismo
    class MotorComun : Motor
    {
        public MotorComun()
        {
            Console.WriteLine("Creando Motor");
        }
        public override void encender()
        {
            Console.WriteLine("Encendiendo Motor Comun");
        }
        public override void acelerar()
        {
            Console.WriteLine("Acelerando Motor Comun");

        }

        public override void apagar()
        {
            Console.WriteLine("Apagando Motor Comun");
        }

    }
}
