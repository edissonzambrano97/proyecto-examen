﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    class MotorEconomico : Motor
    {
        //Declaramos Motor comun e implementamos la clase abstracta Motor para el respectivo funcionamiento del mismo
        public MotorEconomico()
        {
            Console.WriteLine("Creando Motor Economico");
        }
        public override void encender()
        {
            Console.WriteLine("Encendiendo Motor Economico");
        }
        public override void acelerar()
        {
            Console.WriteLine("Acelerando Motor Economico");

        }

        public override void apagar()
        {
            Console.WriteLine("Apagando Motor Economico");
        }

    }
}
