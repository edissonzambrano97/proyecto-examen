﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{

    //Creamos clase Abstracta Motor de la que se implementara en el resto de motores
    abstract class Motor
    {
        abstract public void encender();
        abstract public void acelerar();
        abstract public void apagar();

    }
}
