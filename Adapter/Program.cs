﻿using System;

namespace Adapter
{
    //Generamos la interfaz del usuario para que pueda escoger que motor desea utilizar
    //para llevar a cabo el funcionamiento del vehiculo / dispostivo
    class Programs
    {
        private static Motor motor;
        static void Main(string[] args)
        {
            Console.WriteLine("");
            int opcion;
            do
            {
                opcion = preguntarOpcion();
                switch (opcion)
                {
                    case 1:
                        motor = new MotorComun();
                        usarMotor();
                        break;
                    case 2:
                        motor = new MotorEconomico();
                        usarMotor();
                        break;
                    case 3:
                        motor = new MotorElectricoAdapter();
                        usarMotor();
                        break;
                    case 4:
                        Console.WriteLine("¡Cerrando programa!");
                        break;
                    default:
                        Console.WriteLine("La opción ingresada NO es válida.");
                        break;
                }
                Console.Write("\n\n");
            } while (opcion != 4);
        }

        private static int preguntarOpcion()
        {
            Console.Write(
                    "MENÚ DE OPCIONES\n"
                   + "---- -- --------\n"
                   + "1. Encender motor común.\n"
                   + "2. Encender motor económico.\n"
                   + "3. Encender motor eléctrico.\n"
                   + "4. Salir.\n"
                   + "Seleccione opción: "
            );
            return Int32.Parse(Console.ReadLine());
        }

        private static void usarMotor()
        {
            motor.encender();
            motor.acelerar();
            motor.apagar();
        }
    }
}