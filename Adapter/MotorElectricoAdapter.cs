﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    class MotorElectricoAdapter : Motor
    {

        /// <summary>
        /// Adaptamos el Motor electrico al funcionamineto del motor, 
        /// aqui se implementa ya que no son del mismo tipo pero deben hacer una misma funcionalidad
        /// </summary>
        private MotorElectrico motorElectrico;
        public MotorElectricoAdapter()
        {
            Console.WriteLine("Creando motor eléctrico adapter...");
            this.motorElectrico = new MotorElectrico();
        }

        public override void encender()
        {
            Console.WriteLine("Encendiendo motor eléctrico adapter.");
            this.motorElectrico.conectar();
            this.motorElectrico.activar();
        }
        public override void acelerar()
        {
            Console.WriteLine("Acelerando motor eléctrico adapter.");
            this.motorElectrico.moverMasRapido();
        }

        public override void apagar()
        {
            Console.WriteLine("Apagando motor eléctrico adapter.");
            this.motorElectrico.detener();
            this.motorElectrico.desconectar();
        }

    }
}
