﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    class MotorElectrico
    {
        //Clase MotorElectrico que no implementa la clase Abstracta Motor ya que no lleva el mismo proceso de funcionamiento
        private bool conectado = false;

        //Funciones para el motor electrico / Procesos diferentes
        public MotorElectrico()
        {
            Console.WriteLine("Creando motor electrico...");
            this.conectado = false;
        }

        public void conectar()
        {
            Console.WriteLine("Conectando motor eléctrico.");
            this.conectado = true;
        }

        public void activar()
        {
            if (!this.conectado)
            {
                Console.WriteLine("No se puede activar porque no está conectado el motor eléctrico.");
            }
            else
            {
                Console.WriteLine("Está conectado, activando motor eléctrico.");
            }
        }

        public void moverMasRapido()
        {
            if (!this.conectado)
            {
                Console.WriteLine("No se puede mover rapido el motor eléctrico porque no está conectado.");
            }
            else
            {
                Console.WriteLine("Moviendo más rapido, aumentando voltaje del motor eléctrico.");
            }
        }

        public void detener()
        {
            if (!this.conectado)
            {
                Console.WriteLine("No se puede detener motor eléctrico porque no está conectado.");
            }
            else
            {
                Console.WriteLine("Deteniendo motor eléctrico.");
            }
        }

        public void desconectar()
        {
            Console.WriteLine("Desconectando motor eléctrico.");
            this.conectado = false;
        }
    }
}
